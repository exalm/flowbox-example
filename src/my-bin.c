#include "my-bin.h"

struct _MyBin
{
  GtkBin parent_instance;
};

G_DEFINE_TYPE (MyBin, my_bin, GTK_TYPE_BIN)

static void
my_bin_get_preferred_width (GtkWidget *widget,
                            gint      *min,
                            gint      *nat)
{
  GTK_WIDGET_CLASS (my_bin_parent_class)->get_preferred_width (widget, min, nat);
  if (nat)
    *nat = 360;
}

static void
my_bin_class_init (MyBinClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  widget_class->get_preferred_width = my_bin_get_preferred_width;
}

static void
my_bin_init (MyBin *self)
{
}
