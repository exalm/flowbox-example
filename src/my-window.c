#include "my-config.h"
#include "my-window.h"

#include "my-bin.h"

struct _MyWindow
{
  GtkApplicationWindow  parent_instance;
};

G_DEFINE_TYPE (MyWindow, my_window, GTK_TYPE_APPLICATION_WINDOW)

static void
my_window_class_init (MyWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/my-window.ui");
}

static void
my_window_init (MyWindow *self)
{
  g_type_ensure (MY_TYPE_BIN);
  gtk_widget_init_template (GTK_WIDGET (self));
}
